package lawn;

import lawn.parsing.Parser;
import lawn.writing.Formatter;
import lawn.writing.Writer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class LawnMowerKata {
    public static void main(final String[] args) {
        final List<String> input;
        final var formatter = new Formatter();
        final var writer = new Writer(formatter);
        try {
            final var inputFilePath = Path.of(LawnMowerKata.class.getClassLoader().getResource("input.txt").toURI());
            input = Files.readAllLines(inputFilePath);
            final var lawn = new Parser().parse(input);
            writer.write(lawn.executeMower());
        } catch (final IOException | URISyntaxException e) {
            throw new RuntimeException("Exception when reading the input file " + e);
        }
    }
}
