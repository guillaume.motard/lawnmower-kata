package lawn.parsing;

import lawn.Dimension;
import lawn.Instruction;
import lawn.Lawn;
import lawn.mower.Mover;
import lawn.mower.Orientation;
import lawn.mower.Position;
import lawn.mower.movement.ForwardMovement;
import lawn.mower.movement.RotationMovement;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class Parser {
    private Orientation getOrientation(final char orientation) {
        if (orientation == 'N'){
            return Orientation.NORTH;
        }
        else if (orientation == 'W') {
            return Orientation.WEST;
        }
        else if (orientation == 'S') {
            return Orientation.SOUTH;
        }
        else {
            return Orientation.EAST;
        }
    }

    private List<Instruction> getInstruction(final String instructions) {
        final var sequence = new ArrayList<Instruction>();
        for (var i = 0; i < instructions.length(); i++) {
            final var instruction = instructions.charAt(i);
            if (instruction == 'A') {
                sequence.add(Instruction.FORWARD);
            }
            else if (instruction == 'G') {
                sequence.add(Instruction.LEFT);
            }
            else if (instruction == 'D') {
                sequence.add(Instruction.RIGHT);
            }
        }
        return sequence;
    }

    public Lawn parse(final List<String> input) {
        final var dimensions = input.get(0).split(" ");
        final var dimension = new Dimension(parseInt(dimensions[0]), parseInt(dimensions[1]));

        final var positions = new ArrayList<Position>();
        final var orientations = new ArrayList<Orientation>();
        final var instructions = new ArrayList<List<Instruction>>();

        for (var i = 1; i < input.size(); i++) {
            if (i % 2 == 1) {
                final var mower = input.get(i).split(" ");
                positions.add(new Position(parseInt(mower[0]),parseInt(mower[1])));
                orientations.add(getOrientation(mower[2].charAt(0)));
            }
            else {
                instructions.add(getInstruction(input.get(i)));
            }
        }

        final var forwardMovement = new ForwardMovement();
        final var rotationMovement = new RotationMovement();
        final var mover = new Mover(forwardMovement, rotationMovement);
        return new Lawn(instructions, positions, orientations, dimension, mover);
    }
}
