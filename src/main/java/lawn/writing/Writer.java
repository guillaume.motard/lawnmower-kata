package lawn.writing;

import lawn.mower.MovementResult;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Writer {
    private final Formatter formatter;

    public Writer(final Formatter formatter) {
        this.formatter = formatter;
    }

    public void write(final List<MovementResult> movementResults) {
        try (final var writer = new FileWriter("output.txt")) {
            final var toWrite = formatter.format(movementResults);
            writer.write(toWrite);
        }
        catch (final IOException e) {
            throw new RuntimeException("Exception occurred when writing into the file" , e);
        }
    }

}
