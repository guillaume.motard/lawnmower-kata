package lawn.writing;

import lawn.mower.MovementResult;
import lawn.mower.Orientation;

import java.util.List;

public class Formatter {
    private char convertOrientation(final Orientation orientation) {
        if (orientation == Orientation.EAST) {
            return 'E';
        } else if (orientation == Orientation.WEST) {
            return 'W';
        } else if (orientation == Orientation.NORTH) {
            return 'N';
        } else {
            return 'S';
        }
    }
    public String format(final List<MovementResult> movementResults) {
        final var result = new StringBuilder();
        for (var i = 0; i < movementResults.size(); i++) {
            final var movementResult = movementResults.get(i);
            if (i != movementResults.size() - 1)
                result.append(movementResult.position().x()).append(" ").append(movementResult.position().y()).append(" ").append(convertOrientation(movementResult.orientation())).append("\n");
            else
                result.append(movementResult.position().x()).append(" ").append(movementResult.position().y()).append(" ").append(convertOrientation(movementResult.orientation()));
        }
        return result.toString();
    }
}
