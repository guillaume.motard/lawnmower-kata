package lawn;

import lawn.mower.MovementResult;
import lawn.mower.Mover;
import lawn.mower.Orientation;
import lawn.mower.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Lawn {
    private final List<List<Instruction>> mowerInstructions;
    private final List<Position> mowerPositions;
    private final List<Orientation> mowerOrientations;
    private final Dimension lawnDimension;
    private final Mover mover;


    public Lawn(final List<List<Instruction>> mowerInstructions, final List<Position> mowerPositions, final List<Orientation> mowerOrientations, final Dimension lawnDimension, final Mover mover) {
        this.mowerInstructions = mowerInstructions;
        this.mowerPositions = mowerPositions;
        this.mowerOrientations = mowerOrientations;
        this.lawnDimension = lawnDimension;
        this.mover = mover;
    }

    public List<MovementResult> executeMower() {
        final var executionResults = new ArrayList<MovementResult>();
        for (var mowerIndex = 0; mowerIndex < mowerInstructions.size(); mowerIndex++) {
            final var tmpMowerIndex = mowerIndex;
            final var otherMowersPos = mowerPositions.stream().filter(position -> !position.equals(mowerPositions.get(tmpMowerIndex))).toList();
            final var executionResult = mover.executeSequence(
                    lawnDimension,
                    otherMowersPos,
                    mowerPositions.get(mowerIndex),
                    mowerOrientations.get(mowerIndex),
                    mowerInstructions.get(mowerIndex)
            );
            executionResults.add(executionResult);
        }
        return executionResults;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Lawn lawn)) return false;
        return Objects.equals(mowerInstructions, lawn.mowerInstructions) && Objects.equals(mowerPositions, lawn.mowerPositions) && Objects.equals(mowerOrientations, lawn.mowerOrientations) && Objects.equals(lawnDimension, lawn.lawnDimension);
    }

}
