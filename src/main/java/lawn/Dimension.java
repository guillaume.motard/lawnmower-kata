package lawn;

import lawn.mower.Position;

public record Dimension(int height, int width) {
    public boolean contains(final Position position) {
        return height >= position.y() && width >= position.x() && position.y() >= 0 && position.x() >= 0;
    }
}
