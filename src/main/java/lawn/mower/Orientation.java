package lawn.mower;

public enum Orientation {
    NORTH,
    EAST,
    WEST,
    SOUTH
}
