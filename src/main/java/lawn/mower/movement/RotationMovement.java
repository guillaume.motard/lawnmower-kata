package lawn.mower.movement;

import lawn.Instruction;
import lawn.mower.Orientation;

public class RotationMovement {

    public Orientation rotate(final Orientation orientation, final Instruction instruction) {
        if (instruction == Instruction.LEFT) {
            return turnLeft(orientation);
        } else {
            return turnRight(orientation);
        }
    }

    private Orientation turnRight(final Orientation orientation) {
        return switch (orientation) {
            case NORTH -> Orientation.EAST;
            case SOUTH -> Orientation.WEST;
            case WEST -> Orientation.NORTH;
            case EAST -> Orientation.SOUTH;
        };
    }

    private Orientation turnLeft(final Orientation orientation) {
        return switch (orientation) {
            case SOUTH -> Orientation.EAST;
            case NORTH -> Orientation.WEST;
            case EAST -> Orientation.NORTH;
            case WEST -> Orientation.SOUTH;
        };
    }
}
