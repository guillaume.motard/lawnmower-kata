package lawn.mower.movement;

import lawn.mower.Orientation;
import lawn.mower.Position;

public class ForwardMovement {
    public Position moveForward(final Position position, final Orientation orientation) {
        return switch (orientation) {
            case NORTH -> new Position(position.x(), position.y() + 1);
            case SOUTH -> new Position(position.x(), position.y() - 1);
            case EAST -> new Position(position.x() + 1, position.y());
            case WEST -> new Position(position.x() - 1, position.y());
        };
    }
}
