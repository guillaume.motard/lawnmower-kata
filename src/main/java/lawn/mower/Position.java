package lawn.mower;

public record Position(int x, int y) {
}
