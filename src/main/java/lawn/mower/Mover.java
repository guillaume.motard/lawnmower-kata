package lawn.mower;

import lawn.Dimension;
import lawn.Instruction;
import lawn.mower.movement.ForwardMovement;
import lawn.mower.movement.RotationMovement;

import java.util.List;

public class Mover {
    private final ForwardMovement forwardMovement;
    private final RotationMovement rotationMovement;

    public Mover(final ForwardMovement forwardMovement, final RotationMovement rotationMovement) {
        this.forwardMovement = forwardMovement;
        this.rotationMovement = rotationMovement;
    }

    public MovementResult move(final Instruction instruction, final Position position, final Orientation orientation) {
        return switch (instruction) {
            case FORWARD -> new MovementResult(
                    forwardMovement.moveForward(position,orientation),
                    orientation
            );
            case LEFT, RIGHT -> new MovementResult(
                    position,
                    rotationMovement.rotate(orientation,instruction)
            );
        };
    }

    public MovementResult executeSequence(final Dimension dimension, final List<Position> otherMowersPosition, final Position position, final Orientation orientation, final List<Instruction> sequence) {
        var currentPos = position;
        var currentOrientation = orientation;
        for(final var instruction: sequence) {
            final var movementResult = move(instruction, currentPos, currentOrientation);
            if (canMove(movementResult.position(), dimension, otherMowersPosition)) {
                currentPos = movementResult.position();
                currentOrientation = movementResult.orientation();
            }
        }
        return new MovementResult(currentPos, currentOrientation);
    }

    private boolean canMove(final Position position, final Dimension dimension, final List<Position> otherMowersPosition) {
        return dimension.contains(position) && ! otherMowersPosition.contains(position);
    }
}
