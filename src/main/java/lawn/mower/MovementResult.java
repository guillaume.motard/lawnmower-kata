package lawn.mower;

public record MovementResult(Position position, Orientation orientation) {
}
