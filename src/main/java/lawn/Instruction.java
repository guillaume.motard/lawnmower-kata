package lawn;

public enum Instruction {
    FORWARD,
    LEFT,
    RIGHT
}
