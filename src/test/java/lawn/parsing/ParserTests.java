package lawn.parsing;

import lawn.Dimension;
import lawn.Instruction;
import lawn.Lawn;
import lawn.mower.Mover;
import lawn.mower.Orientation;
import lawn.mower.Position;
import lawn.mower.movement.ForwardMovement;
import lawn.mower.movement.RotationMovement;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserTests {

    @Test
    void shouldReturnGoodLawn() {
        final var forwardMovement = new ForwardMovement();
        final var rotationMovement = new RotationMovement();
        final var mover = new Mover(forwardMovement, rotationMovement);
        final var input = List.of(
                "5 5",
                "1 2 N",
                "GAGAGAGAA",
                "3 3 E",
                "AADAADADDA"
        );
        final var parser = new Parser();
        final var result = parser.parse(input);

        final var expected = new Lawn(
                List.of(
                        List.of(
                                Instruction.LEFT,
                                Instruction.FORWARD,
                                Instruction.LEFT,
                                Instruction.FORWARD,
                                Instruction.LEFT,
                                Instruction.FORWARD,
                                Instruction.LEFT,
                                Instruction.FORWARD,
                                Instruction.FORWARD
                        ),
                        List.of(
                                Instruction.FORWARD,
                                Instruction.FORWARD,
                                Instruction.RIGHT,
                                Instruction.FORWARD,
                                Instruction.FORWARD,
                                Instruction.RIGHT,
                                Instruction.FORWARD,
                                Instruction.RIGHT,
                                Instruction.RIGHT,
                                Instruction.FORWARD
                        )
                ),
                List.of(
                        new Position(1, 2),
                        new Position(3, 3)
                ),
                List.of(
                        Orientation.NORTH,
                        Orientation.EAST
                ),
                new Dimension(5, 5),
                mover
        );
        assertEquals(expected, result);
    }
}
