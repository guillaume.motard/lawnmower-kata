package lawn;

import lawn.mower.MovementResult;
import lawn.mower.Mover;
import lawn.mower.Orientation;
import lawn.mower.Position;
import lawn.mower.movement.ForwardMovement;
import lawn.mower.movement.RotationMovement;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LawnTests {
    @Test
    void shouldMoveEachMowerOneByOne() {
        final var dimension = new Dimension(5, 5);
        final var firstMowerPosition = new Position(3, 1);
        final var secondMowerPosition = new Position(2, 1);
        final var movementInstructions = List.of(Instruction.FORWARD, Instruction.FORWARD);
        final var forwardMovement = new ForwardMovement();
        final var rotationMovement = new RotationMovement();
        final var mover = new Mover(forwardMovement, rotationMovement);
        final var lawn = new Lawn(
                List.of(
                        movementInstructions,
                        movementInstructions
                ),
                List.of(
                        firstMowerPosition,
                        secondMowerPosition
                ),
                List.of(
                        Orientation.EAST,
                        Orientation.NORTH
                ),
                dimension,
                mover
        );


        final var result = lawn.executeMower();

        assertEquals(
                List.of(new MovementResult(new Position(5,1), Orientation.EAST), new MovementResult(new Position(2, 3), Orientation.NORTH)),
                result
        );
    }
}
