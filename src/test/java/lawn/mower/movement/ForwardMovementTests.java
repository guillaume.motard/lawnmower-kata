package lawn.mower.movement;

import lawn.mower.Orientation;
import lawn.mower.Position;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ForwardMovementTests {

    @ParameterizedTest
    @MethodSource("directionsAndResult")
    void updatePositionWhenMovingForward(final Position destination, final Position source, final Orientation orientation) {
        assertEquals(destination, new ForwardMovement().moveForward(source, orientation));
    }

    private static Stream<Arguments> directionsAndResult() {
        final var destination = new Position(1, 1);
        return Stream.of(
                Arguments.of(destination, new Position(1, 0), Orientation.NORTH),
                Arguments.of(destination, new Position(1, 2), Orientation.SOUTH),
                Arguments.of(destination, new Position(0, 1), Orientation.EAST),
                Arguments.of(destination, new Position(2, 1), Orientation.WEST)
        );
    }
}
