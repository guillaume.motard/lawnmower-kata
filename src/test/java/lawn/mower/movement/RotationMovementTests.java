package lawn.mower.movement;

import lawn.Instruction;
import lawn.mower.Orientation;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RotationMovementTests {

    @ParameterizedTest
    @MethodSource("leftOptions")
    void updateOrientationWhenTurningLeft(final Orientation origin, final Orientation destination) {
        final var rotationMovement = new RotationMovement();
        final var result = assertDoesNotThrow(() -> rotationMovement.rotate(origin, Instruction.LEFT));

        assertEquals(destination, result);
    }

    private static Stream<Arguments> leftOptions() {
        return Stream.of(
                Arguments.of(Orientation.SOUTH, Orientation.EAST),
                Arguments.of(Orientation.NORTH, Orientation.WEST),
                Arguments.of(Orientation.WEST, Orientation.SOUTH),
                Arguments.of(Orientation.EAST, Orientation.NORTH)
        );
    }

    @ParameterizedTest
    @MethodSource("rightOptions")
    void updateOrientationWhenTurningRight(final Orientation from, final Orientation to) {
        final var rotationMovement = new RotationMovement();
        final var result = assertDoesNotThrow(() -> rotationMovement.rotate(from, Instruction.RIGHT));

        assertEquals(to, result);
    }

    private static Stream<Arguments> rightOptions() {
        return Stream.of(
                Arguments.of(Orientation.SOUTH, Orientation.WEST),
                Arguments.of(Orientation.NORTH, Orientation.EAST),
                Arguments.of(Orientation.WEST, Orientation.NORTH),
                Arguments.of(Orientation.EAST, Orientation.SOUTH)
        );
    }
}
