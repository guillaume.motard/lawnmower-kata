package lawn.mower;

import lawn.Dimension;
import lawn.Instruction;
import lawn.mower.movement.ForwardMovement;
import lawn.mower.movement.RotationMovement;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MoverTests {

    @Mock
    private ForwardMovement forwardMovement;

    @Mock
    private RotationMovement rotationMovement;

    @InjectMocks
    private Mover mover;


    @Test
    void moveForward() {
        final var position = new Position(1, 1);
        final var orientation = Orientation.NORTH;
        final var updatedPosition = new Position(1, 2);

        when(forwardMovement.moveForward(any(), any())).thenReturn(updatedPosition);

        final var result = mover.move(Instruction.FORWARD, position, orientation);

        assertEquals(new MovementResult(updatedPosition, orientation), result);

        final var order = inOrder(forwardMovement, rotationMovement);
        order.verify(forwardMovement).moveForward(position, orientation);
        order.verifyNoMoreInteractions();
    }


    @Test
    void moveRight() {
        final var position = new Position(1, 1);
        final var orientation = Orientation.SOUTH;
        final var updatedOrientation = Orientation.WEST;

        when(rotationMovement.rotate(any(), any())).thenReturn(updatedOrientation);

        final var result = assertDoesNotThrow(() -> mover.move(Instruction.RIGHT, position, orientation));

        assertEquals(new MovementResult(position, updatedOrientation), result);
        final var order = inOrder(forwardMovement, rotationMovement);
        order.verify(rotationMovement).rotate(orientation, Instruction.RIGHT);
        order.verifyNoMoreInteractions();
    }

    @Test
    void moveLeft() {
        final var position = new Position(1, 1);
        final var orientation = Orientation.SOUTH;
        final var updatedOrientation = Orientation.EAST;

        when(rotationMovement.rotate(any(), any())).thenReturn(updatedOrientation);

        final var result = assertDoesNotThrow(() -> mover.move(Instruction.LEFT, position, orientation));

        assertEquals(new MovementResult(position, updatedOrientation), result);

        final var order = inOrder(forwardMovement, rotationMovement);
        order.verify(rotationMovement).rotate(orientation, Instruction.LEFT);
        order.verifyNoMoreInteractions();
    }

    @Test
    void shouldExecuteTheSequenceForAMowerAndReturnTheRightResult() {
        final var forwardMovement = new ForwardMovement();
        final var rotationMovement = new RotationMovement();
        final var mover = new Mover(forwardMovement, rotationMovement);
        final var dimension = new Dimension(5, 5);
        final var otherMowers = List.<Position>of();
        final var startPosition = new Position(1, 1);
        final var startOrientation = Orientation.EAST;
        final var movements = List.of(
                Instruction.FORWARD,
                Instruction.FORWARD,
                Instruction.RIGHT,
                Instruction.FORWARD
        );

        final var mowerMovementResult = new MovementResult(new Position(3, 0), Orientation.SOUTH);

        final var result = mover.executeSequence(dimension, otherMowers, startPosition, startOrientation, movements);

        assertEquals(mowerMovementResult, result);
    }

    @Test
    void shouldCannotMoveIfPositionIsOutOfTheBounds() {
        final var forwardMovement = new ForwardMovement();
        final var rotationMovement = new RotationMovement();
        final var mover = new Mover(forwardMovement, rotationMovement);
        final var dimension = new Dimension(1, 1);
        final var otherMowers = List.<Position>of();
        final var startPosition = new Position(0, 0);
        final var startOrientation = Orientation.NORTH;
        final var movements = List.of(
                Instruction.FORWARD,
                Instruction.FORWARD,
                Instruction.RIGHT,
                Instruction.FORWARD
        );

        final var mowerMovementResult = new MovementResult(new Position(1, 1), Orientation.EAST);

        final var result = mover.executeSequence(dimension, otherMowers, startPosition, startOrientation, movements);

        assertEquals(mowerMovementResult, result);
    }

    @Test
    void skipAMovementIfPositionIsTakenByAnotherMower() {
        final var forwardMovement = new ForwardMovement();
        final var rotationMovement = new RotationMovement();
        final var mover = new Mover(forwardMovement, rotationMovement);

        final var dimension = new Dimension(5, 5);
        final var otherMowers = List.of(new Position(0, 1));
        final var startPosition = new Position(0, 0);
        final var startOrientation = Orientation.NORTH;
        final var movements = List.of(
                Instruction.FORWARD,
                Instruction.FORWARD,
                Instruction.RIGHT,
                Instruction.FORWARD
        );

        final var result = mover.executeSequence(dimension, otherMowers, startPosition, startOrientation, movements);

        assertEquals(new MovementResult(new Position(1, 0), Orientation.EAST), result);
    }
}
