package lawn.writing;

import lawn.mower.MovementResult;
import lawn.mower.Orientation;
import lawn.mower.Position;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FormatterTests {
    @Test
    void shouldConvertTheMovementResult()
    {
        final var movementResults = List.of(
                new MovementResult(new Position(5,1), Orientation.NORTH),
                new MovementResult(new Position(2,1), Orientation.SOUTH),
                new MovementResult(new Position(3,1), Orientation.EAST),
                new MovementResult(new Position(1,1), Orientation.WEST)
        );
        final var formatter = new Formatter();
        final var result = formatter.format(movementResults);
        assertEquals("""
                5 1 N
                2 1 S
                3 1 E
                1 1 W""", result);
    }
}
