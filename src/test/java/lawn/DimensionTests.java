package lawn;

import lawn.mower.Position;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DimensionTests {

    @Test
    void shouldReturnFalseIfPositionIsNotInArea() {
        final var dimension = new Dimension(5, 5);
        assertFalse(dimension.contains(new Position(1,6)));
    }

    @Test
    void shouldReturnFalseIfPositionHasNegativeCoordinates() {
        final var dimension = new Dimension(5, 5);
        assertFalse(dimension.contains(new Position(-1,6)));
    }

    @Test
    void shouldReturnTrueIfPositionIsInArea() {
        final var dimension = new Dimension(5, 5);
        assertTrue(dimension.contains(new Position(1,1)));
    }

    @Test
    void shouldReturnTrueIfPositionAreInLimitOfArea() {
        final var dimension = new Dimension(1,1);
        assertTrue(dimension.contains(new Position(1, 1)));
    }
}
